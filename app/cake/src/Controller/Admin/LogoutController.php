<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LogoutController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * beforeFilter method
     *
     * To set up access before this contoller is executed.
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\Event $event)
    {
        // If user is authenticated
        if (!is_null($this->Auth->user())) {
            $this->Auth->allow(['index']);
        } else {
            $this->Auth->deny();
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->request->is(['post'])) {
            $this->getRequest()->getSession()->delete('session_verified');
            $this->Flash->success('You are now logged out.');
            return $this->redirect($this->Auth->logout());
        }
    }
}
