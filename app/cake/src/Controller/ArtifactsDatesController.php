<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsDates Controller
 *
 * @property \App\Model\Table\ArtifactsDatesTable $ArtifactsDates
 *
 * @method \App\Model\Entity\ArtifactsDate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsDatesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Dates']
        ];
        $artifactsDates = $this->paginate($this->ArtifactsDates);

        $this->set(compact('artifactsDates'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Date id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsDate = $this->ArtifactsDates->get($id, [
            'contain' => ['Artifacts', 'Dates']
        ]);

        $this->set('artifactsDate', $artifactsDate);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsDate = $this->ArtifactsDates->newEntity();
        if ($this->request->is('post')) {
            $artifactsDate = $this->ArtifactsDates->patchEntity($artifactsDate, $this->request->getData());
            if ($this->ArtifactsDates->save($artifactsDate)) {
                $this->Flash->success(__('The artifacts date has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts date could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsDates->Artifacts->find('list', ['limit' => 200]);
        $dates = $this->ArtifactsDates->Dates->find('list', ['limit' => 200]);
        $this->set(compact('artifactsDate', 'artifacts', 'dates'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Date id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsDate = $this->ArtifactsDates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsDate = $this->ArtifactsDates->patchEntity($artifactsDate, $this->request->getData());
            if ($this->ArtifactsDates->save($artifactsDate)) {
                $this->Flash->success(__('The artifacts date has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts date could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsDates->Artifacts->find('list', ['limit' => 200]);
        $dates = $this->ArtifactsDates->Dates->find('list', ['limit' => 200]);
        $this->set(compact('artifactsDate', 'artifacts', 'dates'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Date id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsDate = $this->ArtifactsDates->get($id);
        if ($this->ArtifactsDates->delete($artifactsDate)) {
            $this->Flash->success(__('The artifacts date has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts date could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
