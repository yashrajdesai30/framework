<?php
namespace App\View\Cell;

use Cake\View\Cell;

class PublicationViewCell extends Cell
{
    public function display($id)
    {
        $this->loadModel('Publications');
        $publication = $this->Publications->get($id, [
            'contain' => [
                'EntryTypes',
                'Journals',
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC']
                    ],
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC']
                    ]
                ]
            ]);

        $this->set(compact('publication'));
    }
}
