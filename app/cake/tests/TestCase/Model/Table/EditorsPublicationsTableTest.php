<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EditorsPublicationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EditorsPublicationsTable Test Case
 */
class EditorsPublicationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EditorsPublicationsTable
     */
    public $EditorsPublications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.editors_publications',
        'app.publications',
        'app.editors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EditorsPublications') ? [] : ['className' => EditorsPublicationsTable::class];
        $this->EditorsPublications = TableRegistry::getTableLocator()->get('EditorsPublications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EditorsPublications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
