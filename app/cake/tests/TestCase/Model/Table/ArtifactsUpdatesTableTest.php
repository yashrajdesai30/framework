<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactsUpdatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactsUpdatesTable Test Case
 */
class ArtifactsUpdatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactsUpdatesTable
     */
    public $ArtifactsUpdates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifacts_updates',
        'app.artifacts',
        'app.update_events'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArtifactsUpdates') ? [] : ['className' => ArtifactsUpdatesTable::class];
        $this->ArtifactsUpdates = TableRegistry::getTableLocator()->get('ArtifactsUpdates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArtifactsUpdates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
